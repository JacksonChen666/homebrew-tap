class Tremc < Formula
  include Language::Python::Virtualenv
  desc "Console client for the BitTorrent client Transmission"
  homepage "https://github.com/tremc/tremc"
  version "0.9.3"
  url "https://github.com/tremc/tremc/archive/refs/tags/#{version}.tar.gz"
  sha256 "9629d1fe3a5a0747563acb15ca904b36ce2984977667d7de25f7786e1189051f"
  license "GPL-3.0-or-later"
  head "https://github.com/tremc/tremc.git"

  option "with-pyperclip", "With system clipboard support (for magnet links)"
  option "with-geoip2", "With guessing of country by IP address"

  depends_on "python"

  def install
    ENV["PREFIX"] = prefix.to_s
    if build.with? "pyperclip"
      resource "pyperclip" do
        url "https://files.pythonhosted.org/packages/a7/2c/4c64579f847bd5d539803c8b909e54ba087a79d01bb3aba433a95879a6c5/pyperclip-1.8.2.tar.gz"
        sha256 "105254a8b04934f0bc84e9c24eb360a591aaf6535c9def5f29d92af107a9bf57"
      end
      resource("pyperclip").stage { system "python", *Language::Python.setup_install_args(libexec/"vendor") }
    end
    if build.with? "geoip2"
      resource "geoip2" do
        url "https://files.pythonhosted.org/packages/e2/2e/5658c3ac06d22e1ec15f6b399c58c98319ec3f51b3ebd0620d9ef91f4de8/geoip2-4.5.0.tar.gz"
        sha256 "b542252e87eb40adc3a2fc0f4e84b514c4c5e04ed46923a3a74d509f25f3103a"
      end
      resource("geoip2").stage { system "python", *Language::Python.setup_install_args(libexec/"vendor") }
    end
    system "make", "install"
  end

  test do
    system "false"
  end
end
