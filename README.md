# JacksonChen666 Tap

## How do I install these formulae?

~~`brew install jacksonchen666/tap/<formula>`~~ Probably not that lol

Or actually, `brew tap jacksonchen666/tap https://gitlab.com/JacksonChen666/homebrew-tap.git` and then `brew install <formula>`.

## Documentation

`brew help`, `man brew` or check [Homebrew's documentation](https://docs.brew.sh).
